package com.eclipserunner.views.impl;

import com.eclipserunner.RunnerPlugin;
import com.eclipserunner.model.ICategoryNode;
import com.eclipserunner.model.ILaunchNode;
import com.eclipserunner.model.ILaunchTypeNode;
import com.eclipserunner.model.IRunnerModel;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.IDebugModelPresentation;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * Launch configuration tree decorator.
 *
 * @author vachacz, bary
 * @author Michael Scharf &lt;michael_scharf_google@scharf.gr&gt; - patch idea
 */
public class LaunchTreeLabelProvider extends LabelProvider {

	private static final String IMG_CATEGORY         = "category.gif";
	private static final String IMG_DEFAULT_CATEGORY = "category-archive.gif";
	private static final String IMG_DECORATION       = "star_min.gif";
	private static final String MISSING_IMG_KEY      = "MISSING";

	private IDebugModelPresentation debugModelPresentation;
	private final IRunnerModel runnerModel;

	public LaunchTreeLabelProvider(IRunnerModel runnerModel) {
		this.debugModelPresentation = DebugUITools.newDebugModelPresentation();
		this.runnerModel = runnerModel;
	}

	@Override
	public String getText(Object element) {
		if (element instanceof ICategoryNode) {
			return ((ICategoryNode) element).getName();
		}
		else if (element instanceof ILaunchNode) {
			ILaunchNode launchConfiguration = (ILaunchNode) element;
			return debugModelPresentation.getText(launchConfiguration.getLaunchConfiguration());
		}
		else if (element instanceof ILaunchTypeNode) {
			return debugModelPresentation.getText(((ILaunchTypeNode) element).getLaunchConfigurationType());
		}
		return debugModelPresentation.getText(element);
	}

	@Override
	public Image getImage(Object element) {
		if (element instanceof ICategoryNode) {
			return getCategoryImage((ICategoryNode) element);
		}
		else if (element instanceof ILaunchNode) {
			return getLaunchConfigurationImage((ILaunchNode) element);
		}
		else if (element instanceof ILaunchTypeNode) {
			return getLaunchConfigurationTypeImage((ILaunchTypeNode) element);
		}
		else {
		    return getMissingImage();
		}
	}

    private Image getMissingImage() {
        final ImageRegistry imageRegistry = RunnerPlugin.getDefault().getImageRegistry();
        Image image = imageRegistry.get(MISSING_IMG_KEY);
        if (image == null) {
            image = ImageDescriptor.getMissingImageDescriptor().createImage();
            imageRegistry.put(MISSING_IMG_KEY, image);
        }
        return image;
    }

	private Image getCategoryImage(ICategoryNode launchConfigurationCategory) {
		if (runnerModel.getDefaultCategoryNode() == launchConfigurationCategory) {
			return createImage(IMG_DEFAULT_CATEGORY);
		} else {
			return createImage(IMG_CATEGORY);
		}
	}

	private Image getLaunchConfigurationImage(ILaunchNode launchConfiguration) {
		Image image = debugModelPresentation.getImage(launchConfiguration.getLaunchConfiguration());
		if (launchConfiguration.isBookmarked()) {
			return overlyBookmarkIcon(image, IMG_DECORATION);
		}
		return image;
	}

	private Image getLaunchConfigurationTypeImage(ILaunchTypeNode typeNode) {
		return debugModelPresentation.getImage(typeNode.getLaunchConfigurationType());
	}

	private Image createImage(String imageFileName) {
		return RunnerPlugin.getDefault().getImage(imageFileName);
	}

	private Image overlyBookmarkIcon(Image sourceImage, String decorationFileName) {
		final RunnerPlugin runnerPlugin = RunnerPlugin.getDefault();
		final ImageRegistry imageRegistry = runnerPlugin.getImageRegistry();
		final String key = sourceImage.toString() + "/" + decorationFileName;
		Image image = imageRegistry.get(key);
		if (image == null) {
	        final ImageDescriptor decorationDescriptor = runnerPlugin.getImageDescriptor(decorationFileName);
	        final DecorationOverlayIcon resultDescriptor =
	                new DecorationOverlayIcon(sourceImage, decorationDescriptor, IDecoration.TOP_RIGHT);
	        image = resultDescriptor.createImage();
	        imageRegistry.put(key, image);
        }
		return image;
	}
}
